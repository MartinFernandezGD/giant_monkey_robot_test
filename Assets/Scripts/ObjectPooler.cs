﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [SerializeField] private GameObject rowPrefab;
    [SerializeField] private GameObject tableDataPrefab;

    Queue<GameObject> rows = new Queue<GameObject>();
    Queue<GameObject> dataObjects = new Queue<GameObject>();
    private static ObjectPooler instance;
    public static ObjectPooler Instance {  get { return instance; } }

    private void Awake()
    {
        if (instance != null)
            Destroy(this);
        else
            instance = this;
    }

    public GameObject GetRow()
    {
        if(rows.Count == 0)
        {
            AddItemToQueue(rowPrefab,rows);
        }
        return rows.Dequeue();
    }

    public GameObject GetDataObject()
    {
        if (dataObjects.Count == 0)
        {
            AddItemToQueue(tableDataPrefab, dataObjects);
        }
        return dataObjects.Dequeue();
    }

    public void ReturnRowToPool(GameObject row)
    {
        row.transform.SetParent(this.transform);
        row.gameObject.SetActive(false);
        rows.Enqueue(row);
    }

    public void ReturnDataToPool(GameObject data)
    {
        data.gameObject.SetActive(false);
        dataObjects.Enqueue(data);
    }

    private void AddItemToQueue(GameObject item, Queue<GameObject> queue)
    {
        GameObject newItem = Instantiate(item);
        newItem.SetActive(false);
        queue.Enqueue(newItem);
    }

}
