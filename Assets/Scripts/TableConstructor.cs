﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TableConstructor : MonoBehaviour
{
    [SerializeField] private Text tableTitle;
    [SerializeField] private GameObject rowPrefab;
    [SerializeField] private GameObject dataPrefab;
    [SerializeField] private Transform scrollContainer;

    private List<GameObject> activeRowList = new List<GameObject>();
    private List<GameObject> activeDataList = new List<GameObject>();
    private Table currentTable;

    public void BuildTableInUI(Table table)
    {
        ResetAllObjects();
        currentTable = table;
        tableTitle.text = currentTable.Title;
        CreateHeader();
        CreateRows();
        Debug.Log("<b>[TableConstructor]</b>Table builded in UI");
    }

    private void ResetAllObjects()
    {
        foreach(GameObject row in activeRowList)
        {
            ObjectPooler.Instance.ReturnRowToPool(row);
        }
        activeRowList.Clear();
        foreach (GameObject data in activeDataList)
        {
            ObjectPooler.Instance.ReturnDataToPool(data);
        }
        activeDataList.Clear();
    }

    private void CreateHeader()
    {
        GameObject newRow = ObjectPooler.Instance.GetRow();
        newRow.transform.SetParent(scrollContainer);
        newRow.SetActive(true);
        activeRowList.Add(newRow);

        foreach (string header in currentTable.Headers)
        {
            SetNewDataObject(newRow.transform, header, FontStyle.Bold);
        }
    }

    private void CreateRows()
    {
        foreach(Data tableData in currentTable.Data)
        {
            GameObject newRow = ObjectPooler.Instance.GetRow();
            newRow.transform.SetParent(scrollContainer);
            newRow.SetActive(true);
            activeRowList.Add(newRow);
            foreach (string header in currentTable.Headers)
            {
                SetNewDataObject(newRow.transform, tableData.container[header], FontStyle.Normal);
            }
        }
    }
    
    private void SetNewDataObject(Transform parent, string text, FontStyle style)
    {
        GameObject newData = ObjectPooler.Instance.GetDataObject();
        newData.transform.SetParent(parent);
        newData.SetActive(true);
        newData.GetComponent<Text>().text = text;
        newData.GetComponent<Text>().fontStyle = style;
        activeDataList.Add(newData);
    }
}
