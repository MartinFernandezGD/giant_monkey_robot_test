﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

public static class JsonReader
{
    static string lastJsonString;
    static string currentJsonString;

    public static Table GetTableFrom(string jsonFileName)
    {
        string jsonPath = string.Format("{0}/{1}", Application.streamingAssetsPath, jsonFileName);
        if (File.Exists(jsonPath))
        {
            ReadFile(jsonPath);
            JSONNode jsonData = JSON.Parse(currentJsonString);
            return FromJsonToTable(jsonData);
        }
        else
        {
            Debug.LogFormat("<b>[JsonReader]</b>Couldn't find fhe file with the following path: {0}",jsonPath);
            return null;
        }
    }
    private static void ReadFile(string jsonPath)
    {
        lastJsonString = currentJsonString;
        string jsonString = File.ReadAllText(jsonPath);
        currentJsonString = jsonString;
    }
    private static Table FromJsonToTable(JSONNode jsonData)
    {
        List<string> headers = new List<string>();
        List<Data> data = new List<Data>();
        Data newData;
        Table newTable;

        string title = jsonData["Title"];
        foreach (JSONNode header in jsonData["ColumnHeaders"])
        {
            headers.Add(header);
        }
        foreach(JSONNode dataContainer in jsonData["Data"])
        {
            newData = new Data();
            foreach(string header in headers)
            {
                newData.container.Add(header, dataContainer[header]);
            }
            data.Add(newData);
        }

        newTable = new Table(title, headers.ToArray(), data.ToArray());
        return newTable;
    }

    public static bool JsonHadChanged(string jsonFileName)
    {
        string jsonPath = string.Format("{0}/{1}", Application.streamingAssetsPath, jsonFileName);
        if (File.Exists(jsonPath))
            ReadFile(jsonPath);
        else
            Debug.LogFormat("<b>[JsonReader]</b>Couldn't find fhe file with the following path: {0}", jsonPath);
        return lastJsonString == currentJsonString ? false : true;
    }
}
