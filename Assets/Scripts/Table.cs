﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Table
{
    private string title;
    private string[] headers;
    private Data[] data;

    #region Properties
    public string Title => title;
    public string[] Headers => headers;
    public Data[] Data => data;
    #endregion

    public Table(string _title, string[] _headers, Data[] _data)
    {
        title = _title;
        headers = _headers;
        data = _data;
    }
}
