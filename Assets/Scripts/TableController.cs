﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TableController : MonoBehaviour
{
    Table currentTable;
    TableConstructor constructor => GetComponent<TableConstructor>();

    [SerializeField]
    private string jsonFileName = "JsonChallenge.json";
    [SerializeField]
    private float timeToRefreshJson = 1f;

    private void Start()
    {
        UpdateTable();
    }

    private void OnEnable()
    {
        StartCoroutine(RefreshTable());
    }

    private void OnDisable()
    {
        StopCoroutine(RefreshTable());
    }

    private void UpdateTable()
    {
        currentTable = JsonReader.GetTableFrom(jsonFileName);
        if(currentTable!=null)
            constructor.BuildTableInUI(currentTable);
        else
            Debug.LogErrorFormat("<b>[TableController]</b>Table is null");
    }

    IEnumerator RefreshTable()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeToRefreshJson);
            if (JsonReader.JsonHadChanged(jsonFileName))
            {
                Debug.Log("<b>[TableController]</b>Json has been chaged");
                UpdateTable();
            }
        }
    }
}
